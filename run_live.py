import cv2
import numpy as np

from torchvision import models
import torch
import json


def run_live():
    with open('imagenet_labels.json') as f:
        labels = np.array(json.load(f))

    device = torch.device("cuda:0" if torch.cuda.is_available() else "cpu")
    cap = cv2.VideoCapture(0)
    net = models.resnet18(True)
    net.eval()
    net.to(device)

    ret = True
    while ret:
        ret, image = cap.read()

        rgb_img = cv2.cvtColor(image, cv2.COLOR_BGR2RGB)
        tensor_img = torch.Tensor(rgb_img.transpose(2, 0, 1) / 255.)

        pred = net(tensor_img[None, :, :, :].to(device))
        print(labels[pred.data.cpu().numpy()])
        cv2.imshow("img", image)
        cv2.waitKey(1)

if __name__ == '__main__':
    run_live()