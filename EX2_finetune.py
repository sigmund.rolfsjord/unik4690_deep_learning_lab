from __future__ import print_function
import time

import torch.optim as optim
from torchvision import datasets, models, transforms
import torch.nn.functional as F
import torch.nn as nn
import torch
import os

class FinetuneNet(nn.Module):
    def __init__(self):
        super(FinetuneNet, self).__init__()
        os.environ['TORCH_MODEL_ZOO'] = 'models/'
        self.base_model = models.resnet18(True)
        for param in self.base_model.parameters():
            param.requires_grad = False
        # TODO: create a final layer

    def forward(self, x):
        x = nn.functional.interpolate(x, (224, 224))
        # TODO: run the image through the network
        return x


def train():
    device = torch.device("cuda:0" if torch.cuda.is_available() else "cpu")

    import data
    trainloader = data.get_data_iterator()
    testloader = iter(data.get_data_iterator(is_test=True, batch_size=128))
    net = FinetuneNet()
    net.to(device)

    criterion = nn.CrossEntropyLoss()
    # TODO: input the parameters you want to optimize over
    optimizer = optim.SGD(, lr=0.001, momentum=0.9)

    for epoch in range(2):
        running_loss = 0.0
        tic = time.time()

        for i, (inputs, labels) in enumerate(trainloader, 0):
            # move data to gpu if available
            inputs, labels = inputs.to(device), labels.to(device)

            # zero the parameter gradients
            optimizer.zero_grad()

            # run the network
            outputs = net(inputs)

            # get the loss
            loss = criterion(outputs, labels)
            # get gradients
            loss.backward()
            # perform optimization
            optimizer.step()

            # print statistics
            running_loss += loss.item()
            if i % 20 == 0:    # print every 20 mini-batches
                print('[%d, %5d] loss: %.3f, time: %.3f' %
                      (epoch + 1, i, running_loss / 20, time.time() - tic))
                running_loss = 0.0
                tic = time.time()
            if i % 200 == 0:    # test every 2000 mini-batches
                inputs_test, labels_test = testloader.__next__()
                inputs_test, labels_test = inputs_test.to(device), labels_test.to(device)
                test_output = net(inputs_test)
                test_loss = criterion(test_output, labels_test)
                test_acc = (test_output.argmax(1) == labels_test).double().mean()
                print('TEST [%d, %5d] loss: %.3f, acc: %.3f' %
                      (epoch + 1, i, test_loss, test_acc))


if __name__ == '__main__':
    train()
