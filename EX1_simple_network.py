import time

import torch.optim as optim
import torch.nn as nn
import torch
import torch.nn.functional as F
import cv2
import numpy as np
import data


class SimpleNet(nn.Module):
    def __init__(self):
        super(SimpleNet, self).__init__()
        # TODO: Initialize the layers of your network
        # You can find different layers in torch.nn (https://pytorch.org/docs/stable/nn.html)

    def forward(self, x, visualise=False):
        # TODO: Run the image through your network
        # Your input should be a [Batch_size x 3 x 32 x 32] sized tensor
        # Your output should be a [Batch_size x 10] sized matrix

        if visualise:
            # TODO: Visualize a layer in your network with cv2.imshow
            pass
        # Return the result of your network
        return torch.zeros((x.shape[0], 10))


def train():
    device = torch.device("cuda:0" if torch.cuda.is_available() else "cpu")

    # Create a dataset
    trainloader = data.get_data_iterator()
    testloader = iter(data.get_data_iterator(is_test=True, batch_size=128))

    net = SimpleNet()
    net.to(device)

    criterion = nn.CrossEntropyLoss()
    optimizer = optim.SGD(net.parameters(), lr=0.01, momentum=0.9)

    for epoch in range(2):
        running_loss = 0.0
        tic = time.time()

        for i, (inputs, labels) in enumerate(trainloader, 0):
            # move data to gpu if available
            # TODO: Visualize the images with cv2.imshow

            inputs, labels = inputs.to(device), labels.to(device)

            # zero the parameter gradients
            optimizer.zero_grad()

            # run the network
            outputs = net(inputs, i%100==0)
            # get the loss
            loss = criterion(outputs, labels)
            # get gradients
            loss.backward()
            # perform optimization
            optimizer.step()

            # print statistics
            running_loss += loss.item()
            if i % 200 == 199:    # print every 200 mini-batches
                print('[%d, %5d] loss: %.3f, time: %.3f' %
                      (epoch + 1, i + 1, running_loss / 200, time.time() - tic))
                running_loss = 0.0
                tic = time.time()
            if i % 2000 == 1999:    # test every 2000 mini-batches
                inputs_test, labels_test = testloader.__next__()
                inputs_test, labels_test = inputs_test.to(device), labels_test.to(device)
                test_output = net(inputs_test)
                test_loss = criterion(test_output, labels_test)
                test_acc = (test_output.argmax(1) == labels_test).double().mean()
                print('TEST [%d, %5d] loss: %.3f, acc: %.3f' %
                      (epoch + 1, i + 1, test_loss, test_acc))



if __name__ == '__main__':
    train()