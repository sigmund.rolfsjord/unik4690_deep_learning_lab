import torch
from torch.autograd import Variable
import cv2
import ssd
import numpy as np

VOC_CLASSES = (  # always index 0
    'aeroplane', 'bicycle', 'bird', 'boat',
    'bottle', 'bus', 'car', 'cat', 'chair',
    'cow', 'diningtable', 'dog', 'horse',
    'motorbike', 'person', 'pottedplant',
    'sheep', 'sofa', 'train', 'tvmonitor')

def live_detect():
    device = torch.device("cuda:0" if torch.cuda.is_available() else "cpu")
    mean = torch.Tensor(np.array((104/256.0, 117/256.0, 123/256.0))[:, np.newaxis, np.newaxis]).to(device)

    def transform(frame):
        frame = cv2.resize(frame, (300, 300))
        x = torch.from_numpy(frame).to(device).permute(2, 0, 1).float()
        x -= mean
        return x.unsqueeze(0)


    def predict(frame):
        height, width = frame.shape[:2]
        x = transform(frame)

        y = net(x)  # forward pass
        detections = y.data
        # scale each detection back up to the image
        scale = torch.Tensor([width, height, width, height])
        for i in range(detections.size(1)):
            j = 0
            while detections[0, i, j, 0] >= 0.6:
                pt = (detections[0, i, j, 1:] * scale).cpu().numpy()
                cv2.rectangle(frame,
                              (int(pt[0]), int(pt[1])),
                              (int(pt[2]), int(pt[3])),
                              (50, 50, 255), 2)
                cv2.putText(frame, VOC_CLASSES[i - 1], (int(pt[0]), int(pt[1])),
                            cv2.FONT_HERSHEY_SIMPLEX, 2, (255, 255, 255), 2, cv2.LINE_AA)
                j += 1
        return frame

    net = ssd.build_ssd('test', 300, 21).to(device)
    net.load_state_dict(torch.load('weights/ssd300_mAP_77.43_v2.pth'))

    cap = cv2.VideoCapture(0)
    ret = True
    while ret:
        ret, frame = cap.read()
        frame = predict(frame)
        key = cv2.waitKey(1)
        # keybindings for display
        if key == ord('p'):  # pause
            while True:
                key2 = cv2.waitKey(1) or 0xff
                cv2.imshow('frame', frame)
                if key2 == ord('p'):  # resume
                    break
        cv2.imshow('frame', frame)

        if key == 27:  # exit
            break


if __name__ == '__main__':
    live_detect()